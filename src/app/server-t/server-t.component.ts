import { Component, OnInit } from '@angular/core';

@Component({
  // selector: '[app-server-t]', /* select by attribute */
  // selector: '.app-server-t', /* select by class */
  /* better select by element */
  selector: 'app-server-t',
  templateUrl: './server-t.component.html',
  styleUrls: ['./server-t.component.css']
})
export class ServerTComponent implements OnInit {
  allowNewServer = false;
  serverCreationStatus = 'No server was created!';
  serverName = 'Testserver';
  serverCrested = false;

  constructor() {
    setTimeout(() => {
      this.allowNewServer = true;
    }, 2000);
  }

  ngOnInit(): void {
  }

/* Event Binding eg: (click) = "onCreateServer" */
  onCreateServer()
  {
    this.serverCrested = true;
    this.serverCreationStatus = 'Server was created! Name is' + this.serverName;
  }

  onUpdateServerName(event: Event)
  {
    this.serverName = (<HTMLInputElement>event.target).value;

  }

}
