import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ServerComponent } from './server/server.component';
import { ServerTComponent } from './server-t/server-t.component';

@NgModule({
  declarations: [
    AppComponent,
    ServerComponent,
    ServerTComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
   // HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
